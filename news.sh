#!/bin/sh

# A really simple newsfeed using:
# ufeed: <https://codeberg.org/sujo/ufeed/>
# dmenu: <https://tools.suckless.org/dmenu/>
# - seperator: https://tools.suckless.org/dmenu/patches/separator/
# and cURL :)

# custom 'tags' can be done by inserting plaintext on a newline
# anything that is not a valid format for cURL (or otherwise results in an error) will be displayed verbatim

urls="$XDG_CONFIG_HOME/news/urls"
cache="$XDG_CACHE_HOME/news/"
feedlist="$cache/feedlist"
tmp="/tmp/news"

rm -f "$feedlist"
while read url
do
	src="$(curl -s "$url" 2>/dev/null)"	
	if [ $? -eq 0 ]
	then
		# actual feed
		printf "%s" "$src" | sed 's/<!\[CDATA\[//g; s/\]\]>//g' > "$tmp"
		title="$(tr -d '[:cntrl:]' < $tmp | recode html..ascii | ufeed -c | cut -f1)"
		# FIXME: this is ugly
		printf "%s\n" "$title" >> "$feedlist"
		recode -q html..ascii < "$tmp" | ufeed > "$cache/$title"
		rm "$tmp"
	else	
		# verbatim
		printf "%s" "$url" > "$cache/$url"	
		# FIXME: this is ugly
		printf "%s\n" "$url" >> "$feedlist"
	fi
done < "$urls"

selection="$(dmenu < $feedlist)"
[ -n "$selection" ] && dmenu -d "\t" < "$cache/$selection"
