#!/bin/sh
 
# Bootleg dock daemon
# Turns off laptop's display and focuses external display
# Resets to laptop's display when external display is unplugged

# You will need to add a udev rule which calls this script - reference your distribution's documentation. 

card=$(xrandr | awk '$2 == "connected" && !/eDP1/{print $1}')
if [ $(cat "/sys/class/drm/card0-$(echo "$card" | sed 's/.$/-&/')/status") = "connected" ]
then
  xrandr --output $card --auto
  xrandr --output eDP1 --off
else 
  xrandr --auto
fi

sleep 1
# update wallpaper
/home/uton/.fehbg
