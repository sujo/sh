#!/bin/sh

# video [downloading] steaming
# requires:
# - yt-dlp (or youtube-dl)
# - mpv
# - (optionally) herbe
# usage: $0 [-d <format>] url

path=""

uexit() {
  echo "usage: $0 [-d <format>] url"
  exit 1
}

case "$#" in
  "1")
    path="$1"
    ;;
  "3")
    if [ "$1" == "-d" ]
    then
      path="$2"
      herbe "downloading $3" "to $2"
      yt-dlp -o $2 "$3"
    else
      uexit
    fi
    ;;
  *)
    uexit
    ;;
esac

herbe "now playing: $path"
mpv "$path"
