#!/bin/sh

# usage: $0 <sv name>

if [ -n "$1" ]
then
	tail -F "/var/log/$1/current"
else
	printf "usage: $0 <sv name>\n"
fi
