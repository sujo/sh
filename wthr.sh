#!/bin/sh
# usage: $0 url
# hint: for US, visit <https://radar.weather.gov/region/conus/standard>
# and search for your region

curl -s $1 > ~/.cache/wthr.gif
ffplay -loglevel quiet -loop 0 ~/.cache/wthr.gif
rm ~/.cache/wthr.gif
