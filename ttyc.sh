#!/bin/sh -e

# set $KFC_{LIGHT|DARK} to desired color schemes

if [ -z "$(command -v kfc)" ]
then
	printf "missing command: kfc\n"
	exit 1
fi

if [ $(date +%H) -lt 18 ]
then
	kfc -Ls $KFC_LIGHT
else
	kfc -s $KFC_DARK
fi
