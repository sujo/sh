sh
==

Collection of general (POSIX compliant) system scripts.

Installing scripts is as simple as `# make <script>`. (sans .sh)

Status
| script                | purpose |
|-----------------------|---------|
| [bat](bat.sh)		| Standalone battery status |
| [spop](spop.sh)	| "Pop" out status information |
| [xst](xst.sh)     | Simple status bar for Xroot-compliant WMs |

Programs
| script                | purpose |
|-----------------------|---------|
| [arun](arun.sh)       | adjust input for GUI or CLI |
| [mplay](mplay.sh)     | video [+downloading] streaming |
| [news](news.sh)	    | select and fetch RSS feeds using ufeed |
| [rws](rws.sh)		    | selects + syncs xdrb |
| [sgrab](sgraph.sh)    | ffmpeg full screen cap |
| [ttyc](ttyc.sh)       | set tty color with [kfc](https://github.com/mcpcpc/kfc) |
| [walsel](walsel.sh)   | select wallpaper through sxiv, synced with rws |
| [wthr](wthr.sh)       | Display (ostensibly weather) gifs |

System
| script                | purpose |
|-----------------------|---------|
| [bdock](bdock.sh)     | Bootleg Dock Daemon; Automatically switches monitor focus when external displays are plugged in |

Automation
| script                | purpose |
|-----------------------|---------|
| [reform](reform.sh)   | Makes 'space substitutions' sane |
| [spt-dlp](spt-dlp.sh) | Downloads csv spotify playlist archives through yt-dlp |
| [blanket](blanket.sh) | Copy two depth directory structure with random names |
| [dedupe](dedupe.sh)   | remove higher order duplicated files |
| [open](open.sh)	| sane `$OPEN` script |
| [nconn](nconn.sh) | run script on specificed network connection |
