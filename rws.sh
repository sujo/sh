#!/bin/sh -e

# random wall paper
# selects + syncs xdrb
# depends:
# - xwallpaper: <https://github.com/stoeckmann/xwallpaper>
# - uwal:	<https://codeberg.org/sujo/uwal>
# usage: $0 [DIR|FILE]

wallpaper=""
RWS_CACHE="$XDG_CACHE_HOME/rws"
LAST="$RWS_CACHE/paper"

[ -n "$1" -a ! -e "$1" ] && printf "usage: $0 [DIR|FILE]" && exit 1


[ -f "$1" ] && wallpaper="$1"
[ -d "$1" ] && wallpaper="$(find $1 -type f | shuf -n 1)"

if [ -z "$wallpaper" ]
then
	wallpaper="$(cat $LAST)"
else
	printf "$wallpaper" > "$LAST"
fi

xwallpaper --zoom "$wallpaper"

# update theme
cache="$RWS_CACHE/$(md5sum < $wallpaper | cut -d' ' -f1)"
[ ! -e "$cache" ] && uwal "$wallpaper" > "$cache"
xrdb -merge "$cache"
