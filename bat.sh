#!/bin/sh
# standalone battery status

bat="$(find /sys/class/power_supply/ -name "BAT*" 2>/dev/null)"

if [ -z "$bat" ]
then
	echo "*:"
	exit 0
fi

level="$(sed 's/[.].*//' $bat/capacity)"
status="$(echo $bat/status)"
state="$(cut -c 1 "$status" | sed -e 's/D/-/' -e 's/C/+/' -Ee 's/F|N/~/')"

echo "$state$level"
