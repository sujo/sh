#!/bin/sh
# Blanket copy -- when you don't care about the file names
# usage: $0 src dest

if [ -z "$1" ] || [ -z "$2" ]
then
  echo "usage: $0 src dest"
  exit 1
fi

for file in $1/*/*.*
do
  stem=$(echo "$file" | tr . \\n | tail -n1)
  cp "$file" "$2/$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c10).$stem"
done
