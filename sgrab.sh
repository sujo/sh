#!/bin/sh
# usage: $0 <filename>

if [ -z "$1" ]
then
  echo "usage: $0 <filename>"
  exit 1
fi

ffmpeg -f x11grab -y -framerate 30 -i :0.0 -c:v libx264 -qp 0 -preset ultrafast -crf 18 $1
