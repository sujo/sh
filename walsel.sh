#!/bin/sh
# wal(paper )sel(ect)
# usage: $0 <wallpaper dir>


if [ -z "$1" ]
then
  echo "usage: $0 <wallpaper dir>"
  exit 1
fi

sxiv -rto $1 | xargs rws -f
xdotool key Super+F5
