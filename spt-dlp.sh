#!/bin/sh

# spotify playlist (export parser + downloader)
# downloads songs from spotify playlist csv archives through yt-dlp 
# you can get csv archives of your playlist using <https://github.com/watsonbox/exportify>
# usage: $0 file.csv

if [ -z $1 ]
then
  echo "usage: $0 file.csv"
  exit 1
fi

# create a folder for this playlist
playlist_name=$(basename -s  .csv $1)
mkdir -p $playlist_name
cd $playlist_name

# the first line usually contains header information
# we exclude it because we're operating on defined standard data
echo "$(tail -n +2 ../$1)" | while read l
do
  echo "$l" | sed 's/\",\"/"\n"/g' > index.txt
  # spotify context are denoted as "spotify:...", we need to strip these   
  sed -i '/spotify:/d' index.txt
   
  track="$(sed -n '1p' index.txt)"
  artist="$(sed -n '2p' index.txt)"  

  yt-dlp ytsearch:"$track by $artist" --max-downloads 1 -q --progress -x --audio-format m4a
  echo "Downloaded> $track by $artist"
done
cd ..
