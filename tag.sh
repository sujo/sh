#!/bin/sh -e

# usage: $0 FILE

[ -z "$1" ] && printf "usage: $0 FILE" && exit 1

printf "title:\t"; read -r title
printf "artist:\t"; read -r artist
printf "album:\t"; read -r album

ffmpeg -i "\"$1\"" \\
	-metadata title="\"$title\"" \\
	-metadata artist="\"$artist\"" \\
	-metadata album="\"$album\"" \\
	-c:a copy "\"$1\""
