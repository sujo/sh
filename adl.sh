#!/bin/sh -e

[ -z "$1" ] && printf "usage: $0 URL" && exit 1

yt-dlp --audio-format best -x "$1"
