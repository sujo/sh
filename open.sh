#!/bin/sh

[ -z "$1" ] && printf "Usage: $0 file\n" && exit 1

case "$(file -b --mime-type "$1")"
in
	"text/"*)
		$EDITOR "$1"
		;;
	"video/"*|"audio/"*)
		mpv "$1"
		;;
	"image/"*)
		sxiv "$1"
		;;
esac
