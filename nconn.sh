#!/bin/sh
#
# n(et) conn -- run executable on successful network connection
# place into /etc/network/if-up.d/ with no extension

if [ -z "$1" ] || [ -z "$2" ]
then
	echo "usage: $0 NETWORK_SSID SCRIPT_PATH"
	exit 0
fi

active="$(ip link show | grep -oP '(\w+)(?=: \<BROADCAST)' -)"
ssid="$(iw "$active" info | grep -oP '(?<=ssid )(\S+)')"

# only consider the specific network
if [ "$ssid" -eq "$1" ]
then
	sh "$2"
fi
