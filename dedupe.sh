#!/bin/sh

# remove higher order duplicate files
# e.g. ./a vs ./dir/a --> ./a is deleted
# equal order duplicates will be ignored
# usage: fdupes -ri dir/ | $0 > conflicts.txt

depth() {
  echo $(echo "$1" | grep -o '/' | wc -l)
}

deepest=""
while IFS= read -r line; do
  if [ -z "$line" ]
  then 
    deepest=""
  else
    if [ $(depth "$line") -gt $(depth "$deepest") ]
    then
      # new deepest
      deepest="$line"
    else
      if [ $(depth "$line") -lt $(depth "$deepest") ]
      then
        rm "$line" 
      fi
    fi
    # ignore equal depth
  fi 
done
