#!/bin/sh

# auto adjust for GUI and CLI

prog=$(dmenu_path | dmenu)

if [ -z "$prog" ]
then
  exit 0
fi

if [ -n "$(ls /usr/share/applications/ | grep ^$prog.desktop)" ]
then 
  $prog &
else 
  # open terminal with prefixed $prog
  args=$(echo -n | dmenu -p "cmd: $prog ")
  id=$(base64 -w0 < /dev/urandom | dd bs=18 count=1 | tr -d '/')
  echo "$($prog $args)" > /tmp/"$id".txt
  
  # we don't care about 'successful' runs
  if [ ! -s /tmp/$id.txt ]
  then
      # you could replace this st string with 
      # `xdg-open`, but I don't really use .desktop
      st -e nvim /tmp/$id.txt &
  fi
fi
