#!/bin/sh

# properly format filenames
# --> All spaces should be represented as '_', not '\ ' or '-'.
# usage: $0 dir

if [ "$1" = "" ]
then
  echo ":: detected root directory... exiting"
  echo "usage: $0 dir"
  exit 1
fi

for file in $1/*
do 
  mv -n "$file" "$(echo "$file" | sed -e 's/ /_/g; s/-/_/g')"
done

