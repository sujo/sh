#!/bin/sh

file="$(find . -name "*.tex" 2>/dev/null)"
[ -n "$1" ] && file="$1"

opendetex "$file" | wc -w
