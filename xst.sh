#!/bin/sh -e

# xst: xstat >> sets xrootname
# usage: $0

while true
do
	## univeral stats ##
	date=$(date "+%a.%d")
	time=$(date +%R)
	sound="$(amixer sget Master | tail -n 1 | sed -nr 's/.*\[([0-9]+%)\].* (\[([a-z]+)\])/\1 \2/p')"

	status="*: $sound >> $date >> $time"
	## laptop specific ##
	if [ $(command -v light) ] && [ $(command -v bat.sh) ]
	then
		backlight=$(light -G | sed 's/[.].*//')
		bat=$(bat.sh)
		status=">: $sound >> $backlight >> $bat >> $date >> $time"
	fi

	$(xsetroot -name "$status")
sleep 1
done
