#!/bin/sh

# ew-tube -> RSS

if [ -z "$1" ]
then
  echo "usage: $0 <youtube channel url>"
  exit 1
fi

# <link rel="canonical" href="<actual url>"> --> www.youtube.com/feeds/videos.xml?channel_id=<actual url>

channel_id="$(curl -s $1 | grep '<link rel="canonical" href=".*">')\n"
echo "www.youtube.com/feeds/videos.xml?channel_id=$channel_id"
