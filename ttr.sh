#!/bin/sh

# time-to-read
# avg. reading speed is 238 wpm
[ -z "$1" ] && printf "usage: $0 FILE\n" && exit 1

words=$(wc -w "$1" | cut -d' ' -f1)
ttr=$(expr $words / 238)
if [ $ttr -eq "0" ]
then
	printf "<0 minute read"
else
	printf "~$ttr minute read"
fi
